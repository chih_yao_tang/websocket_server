#include <OpenNI.h>
#include <opencv.hpp>
#include <highgui.hpp>
#include <iostream>
#include <fstream>
#include <set>
#include <vector>
#include "ONIReader.h"
#define NOMINMAX
#define _WEBSOCKETPP_CPP11_FUNCTIONAL_
#define _WEBSOCKETPP_CPP11_MEMORY_ 
#define _WEBSOCKETPP_CPP11_CHRONO_
#include <websocketpp/server.hpp>
#include <websocketpp/config/asio_no_tls.hpp>
#include "./rapidjson/document.h"
#include "./rapidjson/writer.h"
#include "./rapidjson/stringbuffer.h"
#include "./rapidjson/prettywriter.h"
typedef websocketpp::server<websocketpp::config::asio> server;
// test echo : http://www.websocket.org/echo.html

using websocketpp::connection_hdl;
using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;
using websocketpp::lib::ref;
using namespace std;

bool json_handler(false);
int g_frame_index_controler = 0;
openni::PlaybackControl*		g_pPlaybackControl;
vector<int>	g_vIndex;
vector<float> g_cadidates_depth;
vector<std::pair<int, double>>	g_vFrameTime;
vector<std::pair<int, cv::Point2f>> g_cadidates;
vector<openni::Device*>			g_vpDevices;
vector<openni::VideoStream*>	g_vpDepthStream;
vector<openni::Recorder*>		g_vpRecorder;
vector<openni::VideoFrameRef*>	g_frameRef;
vector<ONIReader*> readers;


class broadcast_server {
public:
	broadcast_server() {
		m_server.init_asio();

		m_server.set_open_handler(bind(&broadcast_server::on_open, this, ::_1));
		m_server.set_close_handler(bind(&broadcast_server::on_close, this, ::_1));
		m_server.set_message_handler(bind(&broadcast_server::on_message, this, ::_1, ::_2));
	}

	void on_open(connection_hdl hdl) {
		m_connections.insert(hdl);
	}

	void on_close(connection_hdl hdl) {
		m_connections.erase(hdl);
	}

	
	void on_message(connection_hdl hdl, server::message_ptr msg) {
		std::cout << "* Recieved request ..." << std::endl;
		
		if (json_handler == true)
		{
			readers.clear();
			std::cout << " -Json parser" << std::endl;

			rapidjson::Document doc_dir;
			rapidjson::Document doc_marix;

			
			
			rapidjson::Document::AllocatorType& localAllocator = doc_dir.GetAllocator();
			
			// 1. 把JSON解析至DOM。
			doc_dir.Parse(msg->get_payload().c_str());
			// 2. 利用DOM作出修改。
			
			
			// utf8 convert to int
			int dir_size(atoi(std::to_string(doc_dir.Size()).c_str()));

			for (int i = 0; i < dir_size; i++)
			{
				std::cout << " -i : " << i << std::endl;
				
				rapidjson::Value array_matrix_elements;
				string element;
				string str_msg("message_");
				string str_mx("matrix_");
				str_msg.append(std::to_string(i).c_str());
				str_mx.append(std::to_string(i));

				string dir_oni(doc_dir[str_msg.c_str()].GetString());
				string dir_mx(doc_dir[str_msg.c_str()].GetString());
				dir_oni.append(".oni").c_str();
				dir_mx.append("_directionR.txt").c_str();
				array_matrix_elements.SetArray();
				
				

				ONIReader* reader = new ONIReader(dir_oni.c_str());
				readers.push_back(reader);
				
				
				std::fstream fp(dir_mx.c_str(), ios::in);
				for (int k = 0; k < 9; k++)
				{
					fp >> element;
					array_matrix_elements.PushBack(rapidjson::Value(atof(element.c_str())), localAllocator);
				}
				fp.close();
				
				
				rapidjson::Value key(str_mx.c_str(), doc_marix.GetAllocator());
				rapidjson::GenericStringBuffer<rapidjson::UTF8<> > buffer;
				rapidjson::Writer<rapidjson::GenericStringBuffer<rapidjson::UTF8<> > > writer(buffer);
				doc_dir.AddMember(key, array_matrix_elements, localAllocator);
				doc_dir.Accept(writer);
				printf("--\n%s\n--\n", buffer.GetString());

			}
			// switch off
			json_handler = false;
		}
		else if (msg->get_payload() == "Json")
		{
			std::cout << " -Json handler" << std::endl;
			json_handler = true;
		}
		else if (msg->get_payload() == "Get device numbers")
		{
			std::cout << " -Get device numbers : " << readers.size() << std::endl;
			m_server.set_access_channels(websocketpp::log::alevel::none);
			m_server.send(hdl, std::to_string(readers.size()), websocketpp::frame::opcode::TEXT);
			//JSONNode n = libjson::parse(msg);
		}
		else if (msg->get_payload() == "Get data")
		{
			std::cout << " -Get data" << std::endl;
			unsigned short* data = new unsigned short[640 * 480 * readers.size()];
			openni::VideoFrameRef depthFrame;
			// log setting : http://www.zaphoyd.com/websocketpp/manual/reference/logging
			m_server.set_access_channels(websocketpp::log::alevel::none);
			
			int frame_cnt = INT_MAX;
			for (int i = 0; i < readers.size(); i++)
				frame_cnt = readers.at(i)->getFrameCnt() < frame_cnt ? readers.at(i)->getFrameCnt() : frame_cnt;
			for (int index = 0; index < frame_cnt; index++)
			{
				int shift_offset = 0; // device 
				for (vector<ONIReader*>::iterator it = readers.begin(); it != readers.end(); it++)
				{
					(*it)->getFrame(&depthFrame);
					cv::Mat image_depth(depthFrame.getHeight(), depthFrame.getWidth(), CV_16UC1, (void*)depthFrame.getData());
					unsigned short tmp_data;
					for (int i = 0; i < 480; i++)
					{
						for (int j = 0; j < 640; j++)
						{
							tmp_data = (unsigned short)image_depth.at<unsigned short>(i, j);
							data[shift_offset + (i * 640 + j)] = tmp_data > 0 && tmp_data < 4000 ? tmp_data : 0;
						}

					}
					shift_offset += 480 * 640;
				}
				boost::this_thread::sleep(boost::posix_time::milliseconds(30));
				m_server.send(hdl, data, readers.size() * 640 * 480 * sizeof(unsigned short), websocketpp::frame::opcode::BINARY);
			}

			delete[] data;
		}
		//for (int frame_cnt = 0; frame_cnt < 140; frame_cnt++)
		//{
		//	
		//	depthFrame = *g_frameRef.at(g_frame_index_controler);
		//	cv::Mat image_depth(depthFrame.getHeight(), depthFrame.getWidth(), CV_16UC1, (void*)depthFrame.getData());
		//	unsigned short tmp_data;
		//	//cout << g_frame_index_controler << endl;
		//	for (int i = 0; i < 480; i++)
		//	{
		//		for (int j = 0; j < 640; j++)
		//		{
		//			//cout << (unsigned short)image_depth.at<unsigned short>(i, j) << endl;
		//			tmp_data = (unsigned short)image_depth.at<unsigned short>(i, j);
		//			data[i * 640 + j] = tmp_data > 0 && tmp_data < 4000 ? tmp_data : 0;
		//		}

		//	}
		//	//boost::this_thread::sleep(boost::posix_time::milliseconds(30));
		//	g_frame_index_controler++;
		//	
		//	m_server.send(hdl, data, 640 * 480 * sizeof(unsigned short), websocketpp::frame::opcode::BINARY);
		//}
		//g_frame_index_controler = 0;
		cout << "* End request ..." << endl;
	}

	void run(uint16_t port) {
		m_server.listen(port);
		m_server.start_accept();
		m_server.run();
	}
private:
	typedef std::set<connection_hdl, std::owner_less<connection_hdl>> con_list;

	server m_server;
	con_list m_connections;
};

int main(int argc, char** argv) {
	

	//string dir(argv[1]);
	
	// 1. Initialize
	openni::Status rc = openni::STATUS_OK;
	openni::OpenNI::initialize();

	// D:\calibration_data\face2face(6m)\2015-06-17-10-13-43\5&1052058D&0&2 
	// D:\calibration_data\face2face(6m)\2015-06-17-10-13-43\7&31627BC6&0&4
	//vector<string> dirs;
	
	//std::cout << argc << endl;
	//for (int i = 0; i < argc - 1; i++)
	//{
	//	dirs.push_back(std::string(argv[i + 1]) + ".oni");
	//	ONIReader* reader = new ONIReader( dirs.back().c_str() );
	//	readers.push_back(reader);
	//}

	//// 2. Open the oni file
	//openni::Device* device = new openni::Device();
	//openni::VideoStream* depthStream = new  openni::VideoStream();

	//stringstream ssin;
	//ssin << dir << ".oni";

	//cout << "* open file ... " << ssin.str() << endl;
	//rc = device->open(ssin.str().c_str());

	//// 3a. Get number of frame
	//g_pPlaybackControl = device->getPlaybackControl();
	//depthStream->create(*device, openni::SENSOR_DEPTH);
	//int  iNumberOfFrames = g_pPlaybackControl->getNumberOfFrames(*depthStream);
	//cout << iNumberOfFrames << endl;
	//g_frameRef.resize(iNumberOfFrames);
	//g_vIndex.resize(iNumberOfFrames);

	//// 3b. Get the frame from the oni file
	//openni::VideoFrameRef depthFrame;
	//openni::VideoMode mMode;
	//mMode.setResolution(640, 480);
	//mMode.setFps(30);
	//mMode.setPixelFormat(openni::PIXEL_FORMAT_DEPTH_1_MM);
	//depthStream->setVideoMode(mMode);
	//depthStream->start();

	// reorder vedio frames
	//for (int i = 0; i < iNumberOfFrames; i++)
	//{
	//	openni::VideoFrameRef* tmpFrame = new openni::VideoFrameRef();
	//	depthStream->readFrame(tmpFrame);
	//	cout << tmpFrame->getFrameIndex() << endl;
	//	//g_frameRef.at(tmpFrame->getFrameIndex() - 1) = tmpFrame;
	//	g_frameRef.at(i) = tmpFrame;
	//	g_vIndex.at(i) = tmpFrame->getFrameIndex();
	//}
	//depthFrame = *g_frameRef.at(0);
	cout << "ready" << endl;
	broadcast_server server;
	server.run(9002);
}