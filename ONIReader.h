#ifndef _ONIREADER_H_
#define _ONIREADER_H_
#include <OpenNI.h>
#include <iostream>
class ONIReader
{

public:
	ONIReader(const char* dir)
	{
		//openni::OpenNI::initialize();
		openni::Status rc;
		this->dir = dir;
		std::cout << " dir : "<< dir << std::endl;
		device		= new openni::Device();
		depthStream = new  openni::VideoStream();
		rc = device->open(this->dir);
		if (rc != openni::STATUS_OK)
			std::cout << "* Device open error !" << std::endl;
		rc = depthStream->create(*device, openni::SENSOR_DEPTH);
		if (rc = openni::STATUS_OK)
			std::cout << "* VideoStream create error !" << std::endl;
		playbackControl = device->getPlaybackControl();
		numberOfFrames = playbackControl->getNumberOfFrames(*depthStream);
		mode.setResolution(640, 480);
		mode.setFps(30);
		mode.setPixelFormat(openni::PIXEL_FORMAT_DEPTH_1_MM);
		depthStream->setVideoMode(mode);
		rc = depthStream->start();
		if (rc = openni::STATUS_OK)
			std::cout << "* VideoStream start error !" << std::endl;
	};
	void getFrame(openni::VideoFrameRef* frame);
	int  getFrameCnt();
	const char* getDir();
	~ONIReader();
private:
	const char* dir;
	int  numberOfFrames;
	openni::Device* device;
	openni::VideoStream* depthStream;
	openni::VideoMode mode;
	openni::PlaybackControl* playbackControl;
};
#endif _ONIREADER_H_